# Copyright 2009, 2010, 2013, 2014 Ingmar Vanhassel
# Copyright 2010 Ali Polatel
# Distributed under the terms of the GNU General Public License v2

SUMMARY="SWIG and JNI bindings allowing Xapian to be used from various other programming languages"
HOMEPAGE="https://www.xapian.org/"
DOWNLOADS="https://oligarchy.co.uk/xapian/${PV}/${PNV}.tar.xz"

LICENCES=""
SLOT="0"
MYOPTIONS=""

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}docs/${PNV}/NEWS"

DEPENDENCIES="
    build:
        app-arch/xz
    build+run:
        dev-db/xapian-core[~${PV}]
"

if ever at_least 1.4.4 ; then
    DEPENDENCIES+="
        build+run:
            dev-lang/ruby:=[>=2.1]
            dev-lang/swig[>=3.0.12][ruby]
    "

    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --without-{lua,php7,python3}
    )
else
    DEPENDENCIES+="
        build+run:
            dev-lang/ruby:=[>=1.8]
            dev-lang/swig[>=1.3.33][ruby]
    "
fi

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    # Don\'t run tests under valgrind if present, takes ages
    VALGRIND=:
    --disable-documentation
    --without-{python,php,tcl,csharp,java}
    # Currently disabled
    # --without-guile
    --with-ruby
)

